# SparkleAI

Quick documentation:

Artificial intelligence for human talk like Cortana or Siri. This is still very on in Alpha so don't expect a lot from this program.  
To run, simply run the "spark.py" file to start. There will be updates soon (not decided).
To test the mods in the file, type in "debug or "debug mode".
You can find the following defined functions in the code:
	-WordToNumGen():
		This generates all the numbers into words from "zero" to "ninety nine". 
		I have put this in a function due to the fact that it does not create variables in the main part of the script. 
		This function is finished and will most likely not receive any updates, thus, I'm wondering if I should put it into another file.
		
	-c():
		Clears the screen if used the console on a Windows computer.
		I have not made a fail safe if used on other operating system machines.
		
	-l_fun():
		This is a file loader.
		
	-Math():
		This is a mathematics function. This function will show you have to solve mathematical problems as well as solve them for you.
		This function is NO WHERE NEAR finished as the show solving function is not yet implemented.
		I plan to implement it into plus, minus, times and division problems. 
		I also plan to implement other functions like trigonometry, Pythagoras, x^2 solving others...
		I want at least one area where my program will be better than 'Siri' or 'Cortana' or that 'Google Now' program.
		
	-inputi():
		This function takes in a string (will remove unwanted signs and strings after a few updates), put it into an array and then transfer all the words like "one" or "ninety nine" into ints. 
		This is also why I made the WordToNumGen() function. 
		This function is also no where near finished. Please be patient as I develop my code!

Unfortunately, that's all the documentation I can give at the moment. 

Special thanks:

I would like to thank the creators of psutil module for python.
This made my development so much easier. I also do not claim the ownership of psutil.
To download psutil go to https://github.com/giampaolo/psutil/archive/master.zip 

Thank you!
